Оконные функции 

пример данных 'employees'

id  name        city    department  salary

11  Дарья       Самара  hr          70
12  Борис       Самара  hr          78
21  Елена       Самара  it          84
22  Ксения      Москва  it          90
23  Леонид      Самара  it          104
24  Марина      Москва  it          104
25  Иван        Москва  it          120
31  Вероника    Москва  sales       96
32  Григорий    Самара  sales       96
33  Анна        Москва  sales       100


Ранжирование 

ntile(n) 

SELECT
    ntile(2) over w as title,
    name, city, department, salary
FROM employees
WINDOW w as(
PARTITION BY city
ORDER BY salary asc
)

result

title   name        city       department  salary

1       Ксения      Москва      it          90
1       Вероника    Москва      sales       96
1       Анна        Москва      sales       100
2       Марина      Москва      it          104
2       Иван        Москва      it          120
1       Дарья       Cамара      hr          70
1       Борис       Самара      hr          78
1       Елена       Самара      it          84
2       Григорий    Самара      sales       96
2       Леонид      Самара      it          104

rank()

SELECT
  rank() OVER w as rank,
  name, department, salary
FROM employees
WINDOW w as (
PARTITION BY department
ORDER BY salary desc)
ORDER BY `rank`
)

result

rank    name        department  salary

1       Борис       hr          78
1       Иван        it          120
1       Анна        sales       100
2       Дарья       hr          70
2       Леонид      it          104
2       Марина      it          104
2       Вероника    sales       96
2       Григорий    sales       96
4       Ксения      it          90
5       Елена       it          84


dense_rank()

SELECT
    dense_rank() OVER w as rank,
    name, city, department, salary
FROM employees
WINDOW w as(
PARTITION BY city
ORDER BY salary desc
)

result

rank    name       city     department  salary

1       Иван       Москва   it          120
2       Марина     Москва   it          104
3       Анна       Москва   sales       100
4       Вероника   Москва   sales       96
5       Ксения     Москва   it          90
1       Леонид     Самара   it          104
2       Григорий   Самара   sales       96
3       Елена      Самара   it          84
4       Борис      Самара   hr          78
5       Дарья      Самара   hr          70


Смещение 

lag() & lead()

SELECT
    name, department, 
    lag(salary, 1) OVER w as prev,
    salary,
    lead(salary, 1) OVER w as next
FROM employees
WINDOW w as (
    ORDER BY salary)

result

name        department  prev    salary  next

Дарья       hr                  70      78
Борис       hr          70      78      84
Елена       it          78      84      90
Ксения      it          84      90      96
Вероника    sales       90      96      96
Григорий    sales       96      96      100
Анна        sales       96      100     104
Леонид      it          100     104     104
Марина      it          104     104     120
Иван        it                  104     120

first_value() & last_value()

SELECT
    name, city, department,
    first_value(salary) OVER w AS low,
    salary,
    last_value(salary) OVER w AS hight
FROM employees
WINDOW w AS (
    PARTITION BY department
    ORDER BY salary
    rows between unbounded preceding and unbounded following)

result

name       city     department  low salary  hight

Дарья      Самара   hr          70  70      78
Борис      Самара   hr          70  78      78
Елена      Самара   it          84  84      120
Ксения     Москва   it          84  90      120
Леонид     Самара   it          84  104     120
Марина     Москва   it          84  104     120
Иван       Москва   it          84  120     120
Вероника   Москва   sales       96  96      100
Григорий   Самара   sales       96  96      100
Анна       Москва   sales       96  100     100

avg() sum() 

SELECT
    name, department, salary,
    COUNT(name) over w AS emp_cnt,
    ROUND(AVG(salary) over w) AS sal_avg,
    ROUND(salary * 100 / ROUND(AVG(salary) OVER w) - 100) AS diff
FROM employees
WINDOW w AS (
    PARTITION BY department
    ORDER BY salary)

result

name       department   salary  emp_cnt sal_avg diff

Дарья      hr           70      2       74      -5
Борис      hr           78      2       74       5
Елена      it           84      5       100     -16
Ксения     it           90      5       100     -10
Леонид     it           104     5       100      4
Марина     it           104     5       100      4
Иван       it           120     5       100      20
Вероника   sales        96      3       97      -1
Григорий   sales        96      3       97      -1
Анна       sales        100     3       97       3

Скользящие агрегаты

SELECT
    id,name,department,salary,
    SUM(salary) OVER w AS total
FROM employees
WINDOW w AS(
    PARTITION BY department
    ORDER BY id
    rows between unbounded preceding and current row)

result

id  name        department  salary  total

11  Дарья       hr          70      70
12  Борис       hr          78      148
21  Елена       it          84      84
22  Ксения      it          90      174
23  Леонид      it          104     278
24  Марина      it          104     382
25  Иван        it          120     502
31  Вероника    sales       96      96
32  Григорий    sales       96      192
33  Анна        sales       100     292

